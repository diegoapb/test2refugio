from django.shortcuts import render
from django.contrib.auth.models import User
from apps.usuario.forms import RegistroForm
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy


# Create your views here.

class RegistroUsuarios(CreateView):
    model = User
    template_name = 'usuario/registrar_usuario.html'
    form_class = RegistroForm
    success_url = reverse_lazy('mascota:mascota_listar')

