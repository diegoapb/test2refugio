from django.conf.urls import url, include

from apps.usuario.views import RegistroUsuarios


urlpatterns = [
    url(r'^registrar/$', RegistroUsuarios.as_view(), name='registrar_usuario'),

]
