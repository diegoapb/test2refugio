from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.core.urlresolvers import reverse_lazy

from django.views.generic import ListView, CreateView, DeleteView, UpdateView

from apps.mascota.forms import MascotaForm
from apps.mascota.models import Mascota

# Create your views here.

def index(request):
    return render(request, 'mascota/index.html')


def mascotaView(request):
    if request.method == 'POST':
        form = MascotaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('mascota:index')
    else:
        form = MascotaForm()

    return render(request, 'mascota/mascota_form.html',{'form':form})

def mascotaList(request):
    mascota = Mascota.objects.all()
    contexto = {'mascotas':mascota}
    return render(request,'mascota/mascota_list.html',contexto)

def mascotaEdit(request,id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'GET':
        form = MascotaForm(instance=mascota)
    else:
        form = MascotaForm(request.POST,instance=mascota)
        if (form.is_valid()):
            form.save()
        return redirect('mascota:mascota_listar')
    return render(request,'mascota/mascota_form.html',{'form':form})

def mascotaDelete(request,id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method=='POST':
        mascota.delete()
        return redirect('mascota:mascota_listar')
    return render(request,'mascota/mascota_delete.html',{'mascota':mascota})

class Mascota_List(ListView):
    model = Mascota
    template_name = 'mascota/mascota_list.html'
    ordering = ['id']



class Mascota_Create(CreateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'mascota/mascota_form.html'
    success_url = reverse_lazy('mascota:mascota_listar')

class Mascota_Update(UpdateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'mascota/mascota_form.html'
    success_url = reverse_lazy('mascota:mascota_listar')

class Mascota_Delete(DeleteView):
    model = Mascota
    template_name = 'mascota/mascota_delete.html'
    success_url = reverse_lazy('mascota:mascota_listar')
