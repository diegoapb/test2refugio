from django.conf.urls import url, include

from apps.mascota.views import index, mascotaView, mascotaList, mascotaEdit, mascotaDelete, Mascota_List, Mascota_Create\
    , Mascota_Update, Mascota_Delete

from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^nuevo$', login_required(Mascota_Create.as_view()), name='mascota_crear'),
    url(r'^lista$', login_required(Mascota_List.as_view()), name='mascota_listar'),
    url(r'^editar/(?P<pk>\d+)/$', login_required(Mascota_Update.as_view()), name='mascota_editar'),
    url(r'^eliminar/(?P<pk>\d+)$',login_required(Mascota_Delete.as_view()), name='mascota_eliminar')
]
